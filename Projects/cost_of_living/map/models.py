# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from datetime import datetime

# Create your models here.
class Map(models.Model):
    title = models.CharField(max_length = 200)
    body = models.TextField()
    created_at = models.DateTimeField(default = datetime.now, blank = True)

