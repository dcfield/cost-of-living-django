# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from .models import Map

# Create your views here.
def index(request):
    # return HttpResponse('Hello from posts')

    maps = Map.objects.all()[:10]

    data = {
        'action': 'Please enter your postcode',
        'map': maps
    }

    return render(request, 'map/index.html', data)